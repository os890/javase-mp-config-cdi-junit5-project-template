/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.test;

import org.eclipse.microprofile.config.Config;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.os890.cdi.test.weld.cdi.TestClassScoped;
import org.os890.cdi.test.weld.cdi.impl.context.TestClassContext;
import org.os890.cdi.test.weld.junit.EnableCdi;
import org.os890.cdi.test.weld.junit.TestConfig;

import javax.inject.Inject;

import java.io.Serializable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@TestConfig(key = "x", configValue = "test")
@TestConfig(key = "answer", configValue = "42")

@EnableCdi
public class SimpleTest {
    @Inject
    private Config config;

    @Inject
    private TestData testData;

    @BeforeAll
    public static void init() {
        System.setProperty("z", "sys");
    }

    @Test
    public void testConfig() {
        assertEquals("test", config.getValue("x", String.class));
        assertEquals("file", config.getValue("y", String.class));
        assertEquals("sys", config.getValue("z", String.class));
        assertEquals(42, config.getValue("answer", Integer.class));
    }

    @Test
    public void testConfigAfterReset() {
        assertEquals("test", config.getValue("x", String.class));
        testData.setValue("test-data");
        assertEquals("test-data", testData.getValue());

        TestClassContext.reset();
        assertEquals(42, config.getValue("answer", Integer.class));
        assertNull(testData.getValue());
    }

    @TestClassScoped
    public static class TestData implements Serializable {
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
