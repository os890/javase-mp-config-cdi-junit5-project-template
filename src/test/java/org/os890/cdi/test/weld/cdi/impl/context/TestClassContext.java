/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.test.weld.cdi.impl.context;

import org.os890.cdi.test.weld.cdi.TestClassScoped;

import javax.enterprise.context.spi.Contextual;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import java.lang.annotation.Annotation;

public class TestClassContext extends AbstractContext {
    private final ContextualStorage contextualStorage;
    private static final ThreadLocal<TestClassContextMetadata> BOOTSTRAPPING_METADATA = new ThreadLocal<>(); //only needed during cdi bootstrapping

    public TestClassContext(BeanManager beanManager) {
        super(beanManager);
        contextualStorage = new ContextualStorage(beanManager, true, isPassivatingScope());
    }

    @Override
    protected ContextualStorage getContextualStorage(Contextual<?> contextual, boolean createIfNotExist) {
        return this.contextualStorage;
    }

    @Override
    public Class<? extends Annotation> getScope() {
        return TestClassScoped.class;
    }

    @Override
    public boolean isActive() {
        return true;
    }

    public static void onBeforeInit(Class testClass) {
        BOOTSTRAPPING_METADATA.set(new TestClassContextMetadata().testClass(testClass));
    }

    public static void init() {
        TestClassContextMetadata bootstrappingMetadata = BOOTSTRAPPING_METADATA.get();
        try {
            CDI.current().select(TestClassContextMetadata.class).get().setTestClass(bootstrappingMetadata.getTestClass());
        } finally {
            BOOTSTRAPPING_METADATA.set(null);
            BOOTSTRAPPING_METADATA.remove();
        }
    }

    public static void reset() {
        TestClassContextMetadata metadata = CDI.current().select(TestClassContextMetadata.class).get();

        ((TestClassContext) CDI.current().getBeanManager().getContext(TestClassScoped.class)).reset(metadata.getTestClass());
    }

    public void reset(Class testClass) {
        clear();
        TestClassContextMetadata metadata = CDI.current().select(TestClassContextMetadata.class).get();
        metadata.setTestClass(testClass);
    }

    public static Class getTestClass() {
        TestClassContextMetadata bootstrappingMetadata = BOOTSTRAPPING_METADATA.get();
        if (bootstrappingMetadata != null) {
            return bootstrappingMetadata.getTestClass();
        }

        return CDI.current().select(TestClassContextMetadata.class).get().getTestClass();
    }

    public void onShutdown() {
        clear();
    }

    private void clear() {
        AbstractContext.destroyAllActive(this.contextualStorage);
    }
}