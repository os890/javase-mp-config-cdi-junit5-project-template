package org.os890.cdi.test.weld.cdi.impl.context;

import org.os890.cdi.test.weld.cdi.TestClassScoped;

import java.io.Serializable;

@TestClassScoped
public class TestClassContextMetadata implements Serializable {
    private Class testClass;

    public Class getTestClass() {
        return testClass;
    }

    public void setTestClass(Class testClass) {
        this.testClass = testClass;
    }

    public TestClassContextMetadata testClass(Class testClass) {
        this.testClass = testClass;
        return this;
    }
}
