/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.test.weld.junit.impl;

import org.junit.jupiter.api.extension.*;
import org.os890.cdi.test.util.AnnotationInstanceProvider;
import org.os890.cdi.test.weld.cdi.CdiContainer;
import org.os890.cdi.test.weld.cdi.CdiContainerLoader;
import org.os890.cdi.test.weld.cdi.ContextControl;
import org.os890.cdi.test.weld.cdi.impl.context.TestClassContext;
import org.os890.cdi.test.weld.junit.TestControl;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.spi.Contextual;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.InjectionTarget;
import javax.inject.Singleton;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

//TODO re-visit ParameterResolver
public class CdiJunit5Extension implements BeforeAllCallback, AfterAllCallback, BeforeEachCallback, AfterEachCallback {
    private static final TestControl DEFAULT_TEST_CONTROL = AnnotationInstanceProvider.of(TestControl.class);

    @Override
    public void beforeAll(ExtensionContext context) {
        try {
            TestClassContext.onBeforeInit(context.getRequiredTestClass());

            CdiContainerLoader.getCdiContainer().boot();
        } finally {
            TestClassContext.init();
        }
    }

    @Override
    public void afterAll(ExtensionContext context) {
        try {
            TestClassContext.reset();
        } finally {
            stopCdiContainer();
        }
    }

    public void stopCdiContainer() {
        CdiContainerLoader.getCdiContainer().shutdown();
    }

    @Override
    public void beforeEach(ExtensionContext context) {
        CdiContainer cdiContainer = CdiContainerLoader.getCdiContainer();
        startScopes(context, cdiContainer.getContextControl());

        injectFields(cdiContainer.getBeanManager(), context.getRequiredTestInstance());
    }

    @Override
    public void afterEach(ExtensionContext context) {
        stopStartedScopes(context);
    }

    private void startScopes(ExtensionContext context, ContextControl contextControl) {
        List<Class<? extends Annotation>> scopeClasses = new ArrayList<Class<? extends Annotation>>();

        TestControl testControl = context.getRequiredTestClass().getAnnotation(TestControl.class);
        if (testControl != null) {
            Collections.addAll(scopeClasses, testControl.startScopes());
        } else {
            Collections.addAll(scopeClasses, DEFAULT_TEST_CONTROL.startScopes());
        }

        List<Class<? extends Annotation>> restrictedScopes = new ArrayList<>();

        //controlled by the container and not supported by weld:
        restrictedScopes.add(ApplicationScoped.class);
        restrictedScopes.add(Singleton.class);

        Stack<Class<? extends Annotation>> activeScopes = JUnitStore.getActiveScopesFromStore(context);
        if (activeScopes == null) {
            activeScopes = new Stack<>();
            JUnitStore.set(context, activeScopes);
        }

        for (Class<? extends Annotation> scopeAnnotation : scopeClasses) {
            if (isRestrictedScope(scopeAnnotation, restrictedScopes)) {
                continue;
            }

            try {
                //force a clean context - TODO discuss onScopeStopped call
                contextControl.stopContext(scopeAnnotation);
                contextControl.startContext(scopeAnnotation);
                activeScopes.add(scopeAnnotation);
            } catch (RuntimeException e) {
                Logger logger = Logger.getLogger(CdiJunit5Extension.class.getName());
                logger.setLevel(Level.SEVERE);
                logger.log(Level.SEVERE, "failed to start scope @" + scopeAnnotation.getName(), e);
            }
        }
    }

    private boolean isRestrictedScope(Class<? extends Annotation> scopeAnnotation, List<Class<? extends Annotation>> restrictedScopes) {
        for (Class<? extends Annotation> restrictedScope : restrictedScopes) {
            if (scopeAnnotation.equals(restrictedScope)) {
                return true;
            }
        }
        return false;
    }

    private void stopStartedScopes(ExtensionContext context) {
        Stack<Class<? extends Annotation>> startedScopes = JUnitStore.getActiveScopesFromStore(context);
        if (startedScopes != null) {
            while (!startedScopes.isEmpty()) {
                Class<? extends Annotation> scopeAnnotation = startedScopes.pop();
                try {
                    CdiContainerLoader.getCdiContainer().getContextControl().stopContext(scopeAnnotation);
                } catch (RuntimeException e) {
                    Logger logger = Logger.getLogger(CdiJunit5Extension.class.getName());
                    logger.setLevel(Level.SEVERE);
                    logger.log(Level.SEVERE, "failed to stop scope @" + scopeAnnotation.getName(), e);
                }
            }
        }
    }

    private static <T> T injectFields(BeanManager beanManager, T instance) {
        if (instance == null) {
            return null;
        } else {
            CreationalContext<T> creationalContext = beanManager.createCreationalContext((Contextual) null);
            AnnotatedType<T> annotatedType = (AnnotatedType<T>) beanManager.createAnnotatedType(instance.getClass());
            InjectionTarget<T> injectionTarget = beanManager.createInjectionTarget(annotatedType);
            injectionTarget.inject(instance, creationalContext);
            return instance;
        }
    }
}
