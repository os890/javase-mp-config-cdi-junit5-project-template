/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.os890.cdi.test.config;

import org.eclipse.microprofile.config.spi.ConfigSource;
import org.os890.cdi.test.weld.cdi.impl.context.TestClassContext;
import org.os890.cdi.test.weld.junit.TestConfig;
import org.os890.cdi.test.weld.junit.TestConfigs;

import java.lang.annotation.Annotation;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestClassConfigSource implements ConfigSource {
    @Override
    public Set<String> getPropertyNames() {
        return testConfigAnnotations()
            .map(e -> ((TestConfig) e).key())
            .collect(Collectors.toSet());
    }

    @Override
    public String getValue(String key) {
        return testConfigAnnotations()
            .filter(a -> ((TestConfig) a).key().equals(key))
            .map(e -> ((TestConfig) e).configValue())
            .findFirst().orElse(null);
    }

    @Override
    public String getName() {
        return TestConfig.class.getName();
    }

    private Stream<Annotation> testConfigAnnotations() {
        return Stream.of(TestClassContext.getTestClass().getAnnotations())
            .filter(e -> TestConfigs.class.isAssignableFrom(e.annotationType()) || TestConfig.class.isAssignableFrom(e.annotationType()))
            .flatMap(e -> {
                    if (TestConfig.class.isAssignableFrom(e.annotationType())) {
                        return Stream.of(e);
                    }
                    return Stream.of(((TestConfigs) e).value());
                }
            );
    }
}
